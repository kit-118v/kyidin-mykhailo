﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using lab2.Models;

namespace lab2
{
    public static class Input
    {
        public static Student AddNewStudent()
        {
            Passport passport = InputPassport();
            UniversityGroup universityGroup = InputUniversityGroup();
            int progress = InputProgress();
            return new Student(passport, universityGroup, progress);
        }

        private static int InputProgress()
        {
            Console.Clear();
            Console.WriteLine("Введите успеваемость студента от 1 - 100:");
            string progress = Console.ReadLine();
            var regexProgress = new Regex("^[1-9][0-9]?$|^100$");
            var matchCollection = regexProgress.Matches(progress);
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Коэффициент успеваемости введён неправильно!";
                progress = RegexCheck(progress, matchCollection, regexProgress, errorMessage);
            }
            
            return Int32.Parse(progress);
        }

        private static UniversityGroup InputUniversityGroup()
        {
            Console.Clear();
            Console.WriteLine("Введи индекс группы:");
            
            var indexRegex = new Regex("[а-яА-Я]");
            string index = Console.ReadLine();
            var matchCollection = indexRegex.Matches(index);
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Индекс группы введен неправильно!";
                index = RegexCheck(index, matchCollection, indexRegex, errorMessage);
            }
            
            Console.Clear();
            Console.WriteLine("Введи название факультета:");
            var facultyRegex = new Regex("\\w+");
            string faculty = Console.ReadLine();
            matchCollection = indexRegex.Matches(faculty);
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Название факультета введено неправильно!";
                index = RegexCheck(faculty, matchCollection, facultyRegex, errorMessage);
            }
            
            Console.Clear();
            Console.WriteLine("Введи название специальности:");
            var specialityRegex = new Regex("\\w+");
            string speciality = Console.ReadLine();
            matchCollection = specialityRegex.Matches(speciality);
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Название специальности введено неправильно!";
                index = RegexCheck(faculty, matchCollection, facultyRegex, errorMessage);
            }

            return new UniversityGroup(index, faculty, speciality);
        }

        private static Passport InputPassport()
        {
            Console.Clear();
            Console.WriteLine("Имя: ");
            var nameRegex = new Regex("[А-Я][а-я]+");
            string name = Console.ReadLine();
            var matchCollection = nameRegex.Matches(name);
            
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Имя введено неправильно, попробуйте снова, пример \"Иван\"";
                name = RegexCheck(name, matchCollection, nameRegex, errorMessage);
            }

            Console.Clear();
            Console.WriteLine("Фамилия: ");
            string surname = Console.ReadLine();
            matchCollection = nameRegex.Matches(surname);
            
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Фамилия введена неправильно, попробуйте снова, пример \"Иванов\"";
                surname = RegexCheck(surname, matchCollection, nameRegex, errorMessage);
            }
            
            Console.Clear();
            Console.WriteLine("Отчество: ");
            string middlename = Console.ReadLine();
            matchCollection = nameRegex.Matches(middlename);
            
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Отчество введено неправильно, попробуйте снова, пример \"Иванович\"";
                middlename = RegexCheck(middlename, matchCollection, nameRegex, errorMessage);
            }
            
            Console.Clear();
            Console.WriteLine("Дата рождения (формат ДД.ММ.ГГГГ): ");
            string inputDate = Console.ReadLine();
            var dobRegex = new Regex("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d$");
            matchCollection = dobRegex.Matches(inputDate);
            DateTime dob;
            
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Дата введена неправильно, попробуйте снова, пример \"15.11.2000\"";
                inputDate = RegexCheck(inputDate, matchCollection, dobRegex, errorMessage);
                dob = DateTime.Parse(inputDate);
            }
            else
            {
                dob = DateTime.Parse(inputDate);
            }

            Console.Clear();
            Console.WriteLine("Дата поступления (формат ДД.ММ.ГГГГ): ");
            string inputReceiptDate = Console.ReadLine();
            var receiptDate = new DateTime();
            var receiptDateRegex = new Regex("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d$");
            matchCollection = receiptDateRegex.Matches(inputReceiptDate);
            
            if (matchCollection.Count == 0)
            {
                string errorMessage = "Дата введена неправильно, попробуйте снова, пример \"15.11.2000\"";
                inputReceiptDate = RegexCheck(inputDate, matchCollection, receiptDateRegex, errorMessage);
                receiptDate = DateTime.Parse(inputReceiptDate);
            }
            else
            {
                receiptDate = DateTime.Parse(inputReceiptDate);
            }

            return new Passport(name, surname, middlename, dob, receiptDate);
        }

        public static string RegexCheck(string inputString, MatchCollection matchCollection, Regex regex, string errorMessage)
        {
            while (matchCollection.Count == 0)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine(errorMessage);
                Console.ResetColor();
                inputString = Console.ReadLine();
                matchCollection = regex.Matches(inputString);
            }

            return inputString;
        }
    }
}