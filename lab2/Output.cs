﻿using System;
using lab2.Models;

namespace lab2
{
    public class Output
    {
        public static void PrintStudent(Student student)
        {
            Console.WriteLine("\n-------------------\n");
            Console.WriteLine(student);
            Console.WriteLine("\n-------------------\n");
        }
    }
}