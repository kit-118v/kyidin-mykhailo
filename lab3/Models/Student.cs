﻿using System;
using System.Text.RegularExpressions;

namespace lab8.Models
{
    public class Student
    {
        private Passport _passport;
        private UniversityGroup _universityGroup;
        private int _progress;

        public Student(Passport passport, UniversityGroup universityGroup, int progress)
        {
            _passport = passport;
            _universityGroup = universityGroup;
            _progress = progress;
        }

        public override string ToString()
        {
            return $"{_passport}\n" +
                   $"{_universityGroup}" +
                   $"Успеваемость %: {_progress.ToString()}";
        }

        public string ToFileString()
        {
            return $"{_passport.Name}|{_passport.Surname}|{_passport.MiddleName}|{_passport.Dob.ToString()}|{_passport.ReceiptDate}|" +
                   $"{_universityGroup.ToFileString()}|{_progress}";
        }
    
    }
}