﻿using System;

namespace lab8.Models
{
    public class UniversityGroup
    {
        private string Index { get; set; }
        private string Faculty { get; set; }
        private string Speciality { get; set; }

        public UniversityGroup(string index, string faculty, string speciality)
        {
            Index = index;
            Faculty = faculty;
            Speciality = speciality;
        }

        public override string ToString()
        {
            return $"Индекс группы: {Index}\nФакультет: {Faculty}\nСпециальность: {Speciality}\n" ;
        }

        public string ToFileString()
        {
            return $"{Index}|{Faculty}|{Speciality}" ;
        }
    }
}