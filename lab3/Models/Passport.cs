﻿using System;

namespace lab8.Models
{
    public class Passport
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public DateTime Dob { get; set; }
        
        public DateTime ReceiptDate { get; set; }

        public Passport(string name, string surname, string middleName, DateTime dob, DateTime receiptDate)
        {
            Name = name;
            Surname = surname;
            MiddleName = middleName;
            Dob = dob;
            ReceiptDate = receiptDate;
        }

        public override string ToString()
        {
            return $"Ф.И.О. студента: {Surname} {Name} {MiddleName}\n" +
                   $"Дата рождения: {Dob}\n" +
                   $"Дата поступления: {ReceiptDate}";
        }
    }
}