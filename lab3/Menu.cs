﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using lab8.Models;

namespace lab8
{
    public static class Menu
    {
        public static void Interface()
        {
            var studentCollection = new StudentsCollection();
            bool switchKey = true;
            int studentIndex = 0;
            
            while (switchKey)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("1 - Добавить студента\n" +
                                  "2 - Сгенерировать студентов\n" +
                                  "3 - Вывести всех студентов\n" +
                                  "4 - Вывести студента по индексу\n" +
                                  "5 - Считать из файла\n" +
                                  "6 - Перезаписать студента \n" +
                                  "7 - Удалить студента\n" +
                                  "0 - Выход\n");
                Console.ResetColor();
                string inputKeystring = Console.ReadLine();
                
                var inputKeyRegex = new Regex("[1,2,3,4,5,6,7,0]");
                var matchCollection = inputKeyRegex.Matches(inputKeystring);
                if (matchCollection.Count != 1)
                {
                    string errorMessage = "Опции под таким номером не существует!";
                    inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
                }

                int inputKey = int.Parse(inputKeystring);
                switch (inputKey)
                {
                    case 1:
                        studentCollection.AddStudent(Input.AddNewStudent());
                        break;

                    case 2:
                        Generator.Generate(5, studentCollection);
                        break;
                    
                    case 3:
                        studentCollection.ShowAll();
                        break;
                    
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Напишите индекс студента");
                        studentIndex = Convert.ToInt32(Console.ReadLine());
                        
                        Console.WriteLine(studentCollection[studentIndex].ToString());
                        Console.ReadLine();
                        
                        break;
                    
                    case 5:
                        FileManager.ReadStContainerFromFile("tmp.txt", studentCollection);
                        break;
                    
                    case 6:
                        Console.WriteLine("Напишите индекс студента: ");
                        studentIndex = Convert.ToInt32(Console.ReadLine());
                        studentCollection.UpdateStudent(studentIndex, Input.AddNewStudent());
                        break;
                    
                    case 7:
                        Console.Clear();
                        Console.WriteLine("Напишите индекс студента, которого хотите удалить.\n0 - Назад.");
                        studentIndex = InputRegex.ConvertInt(Console.ReadLine());
                        studentCollection.DeleteStudent(studentIndex);
                        break;
                    
                    case 0:
                        switchKey = false;
                        break;
                }
            }

            FileManager.WriteStContainerToFile("tmp.txt", studentCollection);
            Console.Clear();
            Console.WriteLine("Goodbye!");
        }

        private static void PrintStudents(List<Student> studentsList)
        {
            Console.Clear();
            studentsList.ForEach(student =>
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(student);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ResetColor();
            });

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Нажмите ENTER чтобы продолжить!");
            Console.ReadLine();
        }
    }
}