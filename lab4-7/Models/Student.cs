﻿using System;
using System.Text.RegularExpressions;

namespace lab8.Models
{
    [Serializable]
    public class Student
    {
        private Passport Passport { get; }
        private UniversityGroup UniversityGroup { get; }
        private int Progress { get; }

        public Passport GetPassport()
        {
            return Passport;
        }

        public UniversityGroup GetUniversityGroup()
        {
            return UniversityGroup;
        }

        public int GetProgress()
        {
            return Progress;
        }

        public Student(Passport passport, UniversityGroup universityGroup, int progress)
        {
            Passport = passport;
            UniversityGroup = universityGroup;
            Progress = progress;
        }

        public override string ToString()
        {
            return $"{Passport}\n" +
                   $"{UniversityGroup}" +
                   $"Успеваемость %: {Progress.ToString()}";
        }

        public string ToFileString()
        {
            return $"{Passport.Name}|{Passport.Surname}|{Passport.MiddleName}|{Passport.Dob.ToString()}|{Passport.ReceiptDate}|" +
                   $"{UniversityGroup.ToFileString()}|{Progress}";
        }

    }
}