﻿using System;
using System.Text;

namespace lab8.Models
{
    public class StudentInfo
    {
        /* Вывод для выбранного студента названия группы (аббревиатура названия факультета,
        номер специальности, год поступления, индекс).
        */
        public void PrintStudentGroupInfo(Student student)
        {
            char studentGroupAbbr = student.GetUniversityGroup().GetFaculty()[0];
            StringBuilder stringBuilder = new StringBuilder($"Аббревиатура группы: {studentGroupAbbr}\n");

            string studentSpeciality = student.GetUniversityGroup().GetSpeciality();
            
            stringBuilder.Append($"Специальность: {studentSpeciality}\n");

            StringBuilder receiptYear = new StringBuilder(student.GetPassport().ReceiptDate.ToShortDateString());
            receiptYear.Remove(0, receiptYear.Length - 4);
            Console.WriteLine(receiptYear);
            stringBuilder.Append($"Специальность: {studentSpeciality}\n");

            string studentGroupIndex = student.GetUniversityGroup().GetIndex();
            studentSpeciality = SpecialtyNumber(studentSpeciality);
            stringBuilder.Append($"Индекс группы: {studentGroupIndex}");
            
            Console.WriteLine(stringBuilder.ToString());
        }
        //"ВТП", "КИТ", "КН", "ПИ"
        static public string SpecialtyNumber(string Faculty)
        {
            string result;
            if (Faculty.Equals("ВТП"))
                result = "123";
            else if (Faculty.Equals("КИТ"))
                result = "122";
            else if (Faculty.Equals("ПИ"))
                result = "121";
            else if (Faculty.Equals("КН"))
                result = "143";
            else
                return Faculty;

            return result;
        }

        /* Вывод для выбранного студента номера курса и семестра на текущий момент.*/
        public void PrintStudentCourseInfo(Student student)
        {
            string DateOfAdmission = student.GetPassport().ReceiptDate.ToShortDateString();
            StringBuilder tmp = new StringBuilder(DateTime.Now.ToShortDateString());
            tmp.Remove(0, 8);                           //что бы получить 2 последние цифры
            int YearNow = Convert.ToInt32(tmp.ToString());

            tmp.Clear();
            tmp.Append(DateTime.Now.ToShortDateString());
            tmp.Remove(5, tmp.Length - 5);
            tmp.Remove(0, 3);
            int MonthNow = Convert.ToInt32(tmp.ToString());

            tmp.Clear();
            tmp.Append(DateOfAdmission);
            tmp.Remove(0, tmp.Length - 2);
            int YearOfAdmission = Convert.ToInt32(tmp.ToString());

            int Course = YearNow - YearOfAdmission;
            int Semestr = Course * 2;
            if (MonthNow > 6)
            {
                Course++;
                Semestr++;
            }

            if (Course <= 4)
                Console.WriteLine($"Этот студент учиться на {Course} курсе, {Semestr} семестр.");
            else
                Console.WriteLine("Этот студент выпустился из универа.\n");
        }
        
        public int AgeInDays(Student student)
        {
            string DateOfBirthday = student.GetPassport().Dob.ToShortDateString();
            StringBuilder tmp = new StringBuilder(DateOfBirthday);
            tmp.Remove(0, tmp.Length - 4);
            int BirthYear = Convert.ToInt32(tmp.ToString());

            tmp.Clear();
            tmp.Append(DateOfBirthday);
            tmp.Remove(2, tmp.Length - 2);
            int BirthDay = Convert.ToInt32(tmp.ToString());

            tmp.Clear();
            tmp.Append(DateOfBirthday);
            tmp.Remove(5, tmp.Length - 5);
            tmp.Remove(0, 3);
            int BirthMonth = Convert.ToInt32(tmp.ToString());

            DateTime birthDate = new DateTime(BirthYear, BirthMonth, BirthDay);


            return (DateTime.Today - birthDate).Days;
        }

        public void PrintAllStudentsTable(StudentsCollection studentsCollection)
        {
            foreach (var student in studentsCollection)
            {
                Console.WriteLine(student.ToFileString());
            }
        }
    }
}
