﻿using System;
using System.Linq;

namespace lab8
{
    public class Filter
    {
        private delegate void Calc(StudentsCollection studentsCollection);
        public static StudentsCollection FilterByGroup(StudentsCollection studentsCollection, string groupName)
        {
            StudentsCollection newStudentsCollection = new StudentsCollection();
            var tmpStudentsList = from student in studentsCollection
                where student.GetUniversityGroup().GetIndex() == groupName
                select student;
            foreach (var student in tmpStudentsList)
            {
                newStudentsCollection.AddStudent(student);
            }

            return newStudentsCollection;
        }

        public static StudentsCollection FilterBySpeciality(StudentsCollection studentsCollection,
            string specialityName)
        {
            StudentsCollection newStudentsCollection = new StudentsCollection();
            var tmpStudentsList = from student in studentsCollection
                where student.GetUniversityGroup().GetSpeciality() == specialityName
                select student;
            foreach (var student in tmpStudentsList)
            {
                newStudentsCollection.AddStudent(student);
            }

            return newStudentsCollection;
        }
        
        public static StudentsCollection FilterByFaculty(StudentsCollection studentsCollection,
            string facultyName)
        {
            StudentsCollection newStudentsCollection = new StudentsCollection();
            var tmpStudentsList = from student in studentsCollection
                where student.GetUniversityGroup().GetFaculty() == facultyName
                select student;
            foreach (var student in tmpStudentsList)
            {
                newStudentsCollection.AddStudent(student);
            }

            return newStudentsCollection;
        }
        
        public static StudentsCollection FilterByProgress(StudentsCollection studentsCollection,
            int progress)
        {
            StudentsCollection newStudentsCollection = new StudentsCollection();
            var tmpStudentsList = from student in studentsCollection
                where student.GetProgress() == progress
                select student;
            foreach (var student in tmpStudentsList)
            {
                newStudentsCollection.AddStudent(student);
            }

            return newStudentsCollection;
        }

        public static void AverageSelectedStudents(StudentsCollection studentsCollection, int index)
        {
            Calc calc;
            if (index == 1)
            {
                calc = CalcAvgAge;
            }
            else
            {
                calc = CalcAvgRating;
            }

            calc(studentsCollection);
        }

        private static void CalcAvgAge(StudentsCollection studentsCollection)
        {
            double avg = 0;
            foreach (var student in studentsCollection)
                avg += Calculations.GetAge(student.GetPassport().Dob);

            Console.WriteLine("Average age = " + avg/studentsCollection.Count());
            Console.ReadLine();
        }
        private static void CalcAvgRating(StudentsCollection studentsCollection)
        {
            double avg = 0;
            foreach (var student in studentsCollection)
                avg += student.GetProgress();

            Console.WriteLine("Average rating = " + avg / studentsCollection.Count());
            Console.ReadLine();
        }

    }
}