﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using lab8.Models;

namespace lab8
{
    public class FileManager
    {
        public static void WriteStContainerToFile(string path, StudentsCollection writtenContainer)
        {

            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {

                    foreach (var i in writtenContainer)
                    {
                        sw.WriteLine(i.ToFileString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static void ReadStContainerFromFile(string path, StudentsCollection readContainer)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });
                        Passport passport = new Passport(infoStudent[0], infoStudent[1], 
                            infoStudent[2], DateTime.Parse(infoStudent[3]), DateTime.Parse(infoStudent[4]));
                        UniversityGroup universityGroup = new UniversityGroup(infoStudent[5], infoStudent[6], infoStudent[7]);
                        readContainer.AddStudent(new Student(passport, universityGroup, Int32.Parse(infoStudent[8])));
                    }
                    Console.WriteLine(readContainer);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public static void Save(StudentsCollection students)
        {
            var path = @"D:\people.dat";
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, students);
                Console.WriteLine("Serialization completed. ");
            }
        }
        
        public static StudentsCollection Load()
        {
            var path = @"D:\people.dat";
            StudentsCollection students;
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                students = (StudentsCollection)formatter.Deserialize(fs);
                Console.WriteLine("Deserialization completed. ");
            }
            return students;
        }

    }
}