﻿using System;
using lab8.Models;

namespace lab8
{
    public class Output
    {
        public static void PrintStudent(Student student)
        {
            Console.WriteLine("\n-------------------\n");
            Console.WriteLine(student);
            Console.WriteLine("\n-------------------\n");
        }
    }
}