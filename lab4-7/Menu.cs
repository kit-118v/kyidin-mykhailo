﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using lab8.Models;

namespace lab8
{
    public static class Menu
    {
        delegate void Calc();
        public static void Interface()
        {
            var studentCollection = new StudentsCollection();
            var studentInfo = new StudentInfo();;
            bool switchKey = true;
            int studentIndex = 0;
            
            while (switchKey)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("1 - Добавить студента\n" +
                                  "2 - Сгенерировать студентов\n" +
                                  "3 - Вывести всех студентов\n" +
                                  "4 - Вывести студента по индексу\n" +
                                  "5 - Считать из файла\n" +
                                  "6 - Перезаписать студента \n" +
                                  "7 - Удалить студента\n" +
                                  "8 - 4ая лаба\n" +
                                  "9 - 5ая лаба\n" +
                                  "10 - Загрузить из файла\n" +
                                  "0 - Выход\n");
                Console.ResetColor();
                string inputKeystring = Console.ReadLine();
                
                var inputKeyRegex = new Regex("[1,2,3,4,5,6,7,8,9,10,0]");
                var matchCollection = inputKeyRegex.Matches(inputKeystring);
                if (matchCollection.Count != 1)
                {
                    string errorMessage = "Опции под таким номером не существует!";
                    inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
                }

                int inputKey = int.Parse(inputKeystring);
                switch (inputKey)
                {
                    case 1:
                        studentCollection.AddStudent(Input.AddNewStudent());
                        break;

                    case 2:
                        Generator.Generate(5, studentCollection);
                        FileManager.Save(studentCollection);
                        break;
                    
                    case 3:
                        studentCollection.ShowAll();
                        break;
                    
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Напишите индекс студента");
                        studentIndex = Convert.ToInt32(Console.ReadLine());
                        
                        Console.WriteLine(studentCollection[studentIndex].ToString());
                        Console.ReadLine();
                        
                        break;
                    
                    case 5:
                        FileManager.ReadStContainerFromFile("tmp.txt", studentCollection);
                        break;
                    
                    case 6:
                        Console.WriteLine("Напишите индекс студента: ");
                        studentIndex = Convert.ToInt32(Console.ReadLine());
                        studentCollection.UpdateStudent(studentIndex, Input.AddNewStudent());
                        break;
                    
                    case 7:
                        Console.Clear();
                        Console.WriteLine("Напишите индекс студента, которого хотите удалить.\n0 - Назад.");
                        studentIndex = InputRegex.ConvertInt(Console.ReadLine());
                        studentCollection.DeleteStudent(studentIndex);
                        break;
                    
                    case 8:
                        Console.Clear();
                        Console.WriteLine("Напишите индекс студента: ");
                        studentIndex = Convert.ToInt32(Console.ReadLine());
                        studentInfo.PrintStudentGroupInfo(studentCollection[studentIndex]);
                        studentInfo.PrintStudentCourseInfo(studentCollection[studentIndex]);
                        studentInfo.AgeInDays(studentCollection[studentIndex]);
                        Console.ReadLine();
                        break;
                    
                    case 9:
                        Laba5Menu(studentCollection);
                        break;
                    
                    case 10:
                        studentCollection = FileManager.Load();
                        break;
                    
                    case 0:
                        switchKey = false;
                        break;
                }
            }

            FileManager.WriteStContainerToFile("tmp.txt", studentCollection);
            Console.Clear();
            Console.WriteLine("Goodbye!");
        }

        private static void Laba5Menu(StudentsCollection studentsCollection)
        {
            StudentInfo studentInfo = new StudentInfo();;
            bool switchKey = true;
            while (switchKey)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("1 - Вывести таблицу студентов.\n" +
                                  "2 - Групповое удаление.\n" +
                                  "3 - Средний возвраст.\n" +
                                  "4 - Средний балл.\n" +
                                  "0 - Назад.");
                Console.ResetColor();

                string inputKeystring = Console.ReadLine();
                
                var inputKeyRegex = new Regex("[1,2,3,4,5,6,7,8,0]");
                var matchCollection = inputKeyRegex.Matches(inputKeystring);
                if (matchCollection.Count != 1)
                {
                    string errorMessage = "Опции под таким номером не существует!";
                    inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
                }

                int inputKey = int.Parse(inputKeystring);
                
                switch (inputKey)
                {
                    case 1:
                        studentInfo.PrintAllStudentsTable(studentsCollection);
                        Console.ReadLine();
                        break;
                    
                    case 2:
                        DeleteStudentsBy(studentsCollection);
                        break;
                        
                    case 3:
                        AverageBy(studentsCollection);
                        break;
                    
                    case 0:
                        switchKey = false;
                        break;
                }
            }

            
        }

        private static void AverageBy(StudentsCollection studentsCollection)
        {
            Calc calc;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1 - По группе.\n" +
                              "2 - По специальности.\n" +
                              "3 - По факультету \n" +
                              "4 - По успеваемости \n" +
                              "0 - Назад.");
            Console.ResetColor();

            string inputKeystring = Console.ReadLine();
                
            var inputKeyRegex = new Regex("[1,2,3,4,5,6,7,8,0]");
            var matchCollection = inputKeyRegex.Matches(inputKeystring);
            if (matchCollection.Count != 1)
            {
                string errorMessage = "Опции под таким номером не существует!";
                inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
            }

            int inputKey = int.Parse(inputKeystring);
            StudentsCollection tmpStudentsCollection = null;
            switch (inputKey)
            {
                case 1:
                    Console.Clear();
                    Console.WriteLine("Введите название группы:");
                    string groupName = Console.ReadLine();
                    tmpStudentsCollection = Filter.FilterByGroup(studentsCollection, groupName);
                    AverageChoose(tmpStudentsCollection);
                    break;
                
                case 2:
                    Console.Clear();
                    Console.WriteLine("Введите название специальности:");
                    string specialityName = Console.ReadLine();
                    tmpStudentsCollection = Filter.FilterBySpeciality(studentsCollection, specialityName);
                    AverageChoose(tmpStudentsCollection);
                    break;
                
                case 3:
                    Console.Clear();
                    Console.WriteLine("Введите название факультета:");
                    string facultyName = Console.ReadLine();
                    tmpStudentsCollection = Filter.FilterByFaculty(studentsCollection, facultyName);
                    AverageChoose(tmpStudentsCollection);
                    break;
                
                case 4:
                    Console.Clear();
                    Console.WriteLine("Введите название факультета:");
                    int progress = InputRegex.ConvertInt(Console.ReadLine());
                    tmpStudentsCollection = Filter.FilterByProgress(studentsCollection, progress);
                    AverageChoose(tmpStudentsCollection);
                    break;
                
                case 0:
                    return;
                
            }
        }

        private static void AverageChoose(StudentsCollection studentsCollection)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1 - Средний возвраст\n" +
                              "2 - Средний балл\n" + 
                              "0 - Назад.");
            Console.ResetColor();

            string inputKeystring = Console.ReadLine();
                
            var inputKeyRegex = new Regex("[1,2,3,4,5,6,7,8,0]");
            var matchCollection = inputKeyRegex.Matches(inputKeystring);
            if (matchCollection.Count != 1)
            {
                string errorMessage = "Опции под таким номером не существует!";
                inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
            }

            int inputKey = int.Parse(inputKeystring);

            switch (inputKey)
            {
                case 1:
                    Filter.AverageSelectedStudents(studentsCollection, 1);
                    break;
                
                case 2:
                    Filter.AverageSelectedStudents(studentsCollection, 2);
                    break;
                
                case 0:
                    return;
                    
            }
        }

        private static void AverageAgeByGroup(StudentsCollection studentsCollection, string groupName)
        {
            
        }

        private static void DeleteStudentsBy(StudentsCollection studentsCollection)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1 - По группе.\n" +
                              "2 - По специальности.\n" +
                              "3 - По факультету \n" +
                              "4 - По успеваемости \n" +
                              "0 - Назад.");
            Console.ResetColor();

            string inputKeystring = Console.ReadLine();
                
            var inputKeyRegex = new Regex("[1,2,3,4,5,6,7,8,0]");
            var matchCollection = inputKeyRegex.Matches(inputKeystring);
            if (matchCollection.Count != 1)
            {
                string errorMessage = "Опции под таким номером не существует!";
                inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
            }

            int inputKey = int.Parse(inputKeystring);
            
            switch (inputKey)
            {
                case 1:
                    Console.WriteLine("Введите номер группы");
                    string groupName = Console.ReadLine();
                    studentsCollection.DeleteByGroup(groupName);
                    break;
                    
                case 2:
                    Console.WriteLine("Введите специальность");
                    string specialityName = Console.ReadLine();
                    studentsCollection.DeleteBySpeciality(specialityName);
                    break;
                
                case 3:
                    Console.WriteLine("Введите факультет");
                    string facultyName = Console.ReadLine();
                    studentsCollection.DeleteByFaculty(facultyName);
                    break;
                
                case 4:
                    Console.WriteLine("Введите успеваемость");
                    int progressCount = Int32.Parse(Console.ReadLine());
                    studentsCollection.DeleteByProgress(progressCount);
                    break;
                
                case 0:
                    return;
            }
        }

        private static void PrintStudents(List<Student> studentsList)
        {
            Console.Clear();
            studentsList.ForEach(student =>
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(student);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ResetColor();
            });

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Нажмите ENTER чтобы продолжить!");
            Console.ReadLine();
        }
    }
}