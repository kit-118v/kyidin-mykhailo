﻿using System;

namespace lab8
{
    public class InputRegex
    {
        public static int ConvertInt(string inputString)
        {
            int result = 0;
            while(!Int32.TryParse(inputString, out result))
            { 
                Console.Clear();
                Console.WriteLine("Неверный формат ввода.\nПример: 1\nВвод: ");
                inputString = Console.ReadLine();
            }
            
            return result;
        }
    }
}