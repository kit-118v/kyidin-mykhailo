﻿using System;
using System.Linq;
using lab8.Models;

namespace lab8
{
    public class Calculations
    {
        public static int GetAge(DateTime date)
        {
            DateTime today = DateTime.Today;
            TimeSpan old = today.Subtract(date);
            var d = new DateTime(old.Ticks);

            return d.Year - 1;
        }
        
        
    }
}