﻿using Microsoft.AspNetCore.Mvc;

namespace WebApplication1
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var students = new StudentsCollection();
            Generator.Generate(10, students);
            return View(students);
        }
    }
}
