﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using WebApplication1.Models;

namespace WebApplication1
{
    [Serializable]
    public class StudentsCollection : IEnumerable<Student>, IEnumerator<Student>
    {
        private Student[] _students;
        private int _index = -1;

        public StudentsCollection()
        {
            _students = new Student[0];
        }

        public Student this[int index]
        {
            get
            {
                return _students[index];
            }
        }

            public void AddStudent(Student newStudent)
        {
            Student[] tmpStudentArray = new Student[_students.Length + 1];

            for(int i = 0; i < _students.Length; i++)
            {
                tmpStudentArray[i] = _students[i];
            }

            tmpStudentArray[_students.Length] = newStudent;

            _students = tmpStudentArray;
        }

        public void DeleteStudent(int index)
        {
            Student[] tmpStudentArray = new Student[_students.Length - 1];

            for (int i = 0; i < index; i++)
            {
                tmpStudentArray[i] = _students[i];
            }

            for (int i = index; i < _students.Length - 1; i++)
            {
                tmpStudentArray[i] = _students[i + 1];
            }

            _students = tmpStudentArray;
        }

        public void UpdateStudent(int index, Student newStudent)
        {
            Student[] tmpStudentArray = new Student[_students.Length - 1];
            
            for (int i = 0; i < index; i++)
            {
                tmpStudentArray[i] = _students[i];
            }

            tmpStudentArray[index] = newStudent;
            
            for (int i = index+1; i < _students.Length - 1; i++)
            {
                tmpStudentArray[i] = _students[i + 1];
            }

            _students = tmpStudentArray;
        }

        public void MultiDeleteStudent(List<int> studentsIndexList)
        {
            if (_students.Length == studentsIndexList.Count)
            {
                _students = new Student[0];
                return;
            }
                
            Student[] newStudents = new Student[_students.Length - studentsIndexList.Count];
            int counter = 0;
            int counterStudentsIndex = 0;
            for (var i = 0; i < _students.Length; i++)
            {
                if (counterStudentsIndex < studentsIndexList.Count && i == studentsIndexList[counterStudentsIndex])
                {
                    counterStudentsIndex++;
                    continue;
                }
                newStudents[counter] = _students[i];
                counter++;
            }

            _students = newStudents;
        }

        public void DeleteByGroup(string groupName)
        {
            List<int> studentsIndexToDelete = new List<int>();

            foreach (var student in _students.Select((element, index) => new { student = element, index = index}))
            {
                if (student.student.GetUniversityGroup().GetIndex() == groupName)
                {
                    studentsIndexToDelete.Add(student.index);
                }
            }
            
            MultiDeleteStudent(studentsIndexToDelete);
        }
        
        public void DeleteBySpeciality(string specialityName)
        {
            List<int> studentsIndexToDelete = new List<int>();

            foreach (var student in _students.Select((element, index) => new { student = element, index = index}))
            {
                if (student.student.GetUniversityGroup().GetSpeciality() == specialityName)
                {
                    studentsIndexToDelete.Add(student.index);
                }
            }
            
            MultiDeleteStudent(studentsIndexToDelete);
        }

        public void DeleteByFaculty(string facultyName)
        {
            List<int> studentsIndexToDelete = new List<int>();

            foreach (var student in _students.Select((element, index) => new { student = element, index = index}))
            {
                if (student.student.GetUniversityGroup().GetFaculty() == facultyName)
                {
                    studentsIndexToDelete.Add(student.index);
                }
            }
            
            MultiDeleteStudent(studentsIndexToDelete);
        }

        public void DeleteByProgress(int progressCount)
        {
            List<int> studentsIndexToDelete = new List<int>();

            foreach (var student in _students.Select((element, index) => new { student = element, index = index}))
            {
                if (student.student.GetProgress() == progressCount)
                {
                    studentsIndexToDelete.Add(student.index);
                }
            }

            MultiDeleteStudent(studentsIndexToDelete);
        }
        
        public IEnumerator<Student> GetEnumerator()
        {
            return this;
        }

        public bool MoveNext()
        {
            if (_index == _students.Length - 1)
            {
                Reset();
                return false;
            }

            _index++;
            return true;
        }

        public void Reset()
        {
            _index = -1;
        }

        public void Dispose()
        {
            Reset();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        public Student Current
        {
            get
            {
                return _students[_index];
            }
        }

        object IEnumerator.Current => _students[_index];
    }
}