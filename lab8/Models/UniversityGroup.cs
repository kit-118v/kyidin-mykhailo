﻿using System;

namespace WebApplication1.Models
{
    [Serializable]
    public class UniversityGroup
    {
        private string Index { get; set; }
        private string Faculty { get; set; }
        private string Speciality { get; set; }

        public string GetIndex()
        {
            return Index;
        }

        public string GetFaculty()
        {
            return Faculty;
        }

        public string GetSpeciality()
        {
            return Speciality;
        }

        public UniversityGroup(string index, string faculty, string speciality)
        {
            Index = index;
            Faculty = faculty;
            Speciality = speciality;
        }

        public override string ToString()
        {
            return $"Индекс группы: {Index}\nФакультет: {Faculty}\nСпециальность: {Speciality}\n" ;
        }

        public string ToFileString()
        {
            return $"{Index}|{Faculty}|{Speciality}" ;
        }
    }
}