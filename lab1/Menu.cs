﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using lab1.Models;

namespace lab1
{
    public static class Menu
    {
        public static void Interface()
        {
            var studentsList = new List<Student>();
            bool switchKey = true;
            
            while (switchKey)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("1 - Добавить студента\n" +
                                  "2 - Сгенерировать студентов\n" +
                                  "3 - Вывести всех студентов\n" +
                                  "0 - Выход\n");
                Console.ResetColor();
                string inputKeystring = Console.ReadLine();
                
                var inputKeyRegex = new Regex("[1,2,3,0]");
                var matchCollection = inputKeyRegex.Matches(inputKeystring);
                if (matchCollection.Count != 1)
                {
                    string errorMessage = "Опции под таким номером не существует!";
                    inputKeystring = Input.RegexCheck(inputKeystring, matchCollection, inputKeyRegex, errorMessage);
                }

                int inputKey = int.Parse(inputKeystring);
                switch (inputKey)
                {
                    case 1:
                        Input.AddNewStudent(studentsList);
                        break;

                    case 2:
                        studentsList = Generator.Generate(5);
                        break;
                    
                    case 3:
                        PrintStudents(studentsList);
                        break;

                    case 0:
                        switchKey = false;
                        break;
                }
            }
        }

        private static void PrintStudents(List<Student> studentsList)
        {
            Console.Clear();
            studentsList.ForEach(student =>
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(student);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ResetColor();
            });

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Нажмите ENTER чтобы продолжить!");
            Console.ReadLine();
        }
    }
}