﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using lab1.Models;

namespace lab1
{
    public class DoubleLinkedList<T> {  
        
        internal Node<T> head;

        public DoubleLinkedList(T data)
        {
            head = new Node<T>(data);
        }
    }

    public class Node<T>
    {
        internal T Data;
        internal Node<T> Next;
        internal Node<T> Prev;

        public Node(T newData)
        {
            Data = newData;
            Next = null;
            Prev = null;
        }

        public Node(T newData, Node<T> newNext, Node<T> newPrev)
        {
            Data = newData;
            Next = newNext;
            Prev = newPrev;
        }
    }
    
    public class Collection<T>
    {
        public DoubleLinkedList<T> LinkedList;
        public int Length;
        
        public Collection(T data)
        {
            LinkedList = new DoubleLinkedList<T>(data);
            Length = 1;
        }

        public void InsertFront(T newData)
        {
            Node<T> newNode = new Node<T>(newData);
            newNode.Next = LinkedList.head;
            newNode.Prev = null;  
            if (LinkedList.head != null) {  
                LinkedList.head.Prev = newNode;  
            }
            LinkedList.head = newNode;
        }

        public void InsetLast(T newData)
        {
            Node<T> newNode = new Node<T>(newData);
            
            if (LinkedList.head == null) {    
                LinkedList.head = newNode;    
                return;    
            }

            Node<T> lastNode = GetLastNode();
            lastNode.Next = newNode;
            newNode.Prev = lastNode;
        }

        public void InsertByIndex(int index, T newData)
        {
            if (index < 0 || index >= Length)
            {
                Console.Error.WriteLine("Invalid index!");
                return;
            }
            Node<T> newNode = new Node<T>(newData);
            Node<T> nodeByIndex = GetNodeByIndex(index);
            
        }

        public Node<T> GetNodeByIndex(int index)
        {
            Node<T> tmpNode = LinkedList.head;
            int counter = 0;
            
            while (counter < index && tmpNode.Next != null)
            {
                tmpNode = tmpNode.Next;
            }

            return tmpNode;
        }

        public Node<T> GetLastNode()
        {
            Node<T> tmpNode = LinkedList.head;
            while (tmpNode.Next != null)
            {
                tmpNode = tmpNode.Next;
            }

            return tmpNode;
        }
        
    }
}