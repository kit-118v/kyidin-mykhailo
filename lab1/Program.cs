﻿using System;
using System.Collections.Generic;
using lab1.Models;
using static lab1.Menu;

namespace lab1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // Test.TestMain();
            Interface();
            Console.WriteLine("Hello world!");
            List<Student> studentsList = new List<Student>();
            Input.AddNewStudent(studentsList);
            studentsList.ForEach(student =>
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(student);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("-----------------------------");
                Console.ResetColor();
            });

        }
    }
}