﻿using System;
using System.Collections.Generic;
using lab1.Models;

namespace lab1
{
    public class Generator
    {
        public static List<Student> Generate(int count)
        {
            var studentList = new List<Student>();
            string[] randomNames = { "Вася", "Ваня", "Миша"};
            string[] randomSurnames = {"Иванов", "Сергеев", "Петров", "Детров"};
            string[] randomMiddlename = {"Иванович", "Сергеевич", "Петрович", "Юрьевич"};
            string[] randomDob = {"15.11.2000", "17.03.2001", "08.07.1975", "30.01.1991"};
            string[] randomReceiptDate = {"13.05.2010", "11.08.2021", "09.05.1945", "30.01.2007"};
            string[] randomIndex = {"ВТП", "КИТ", "КН", "ПИ"};
            string[] randomFaculty = {"Кибербезопастность", "Компьютерная инженерия", "Веб-разработка", "Физмат"};
            string[] randomSpeciality =
                {"Системное программирвоание", "Разработка мобильных приложений", "Веб-дизайнер", "Инженер"};
            
            var rnd = new Random();
 
            for (int i = 0; i < count; i++)
            {
                string name = randomNames[rnd.Next(0, 3)];
                string surname = randomSurnames[rnd.Next(0, 3)];
                string middlename = randomMiddlename[rnd.Next(0, 3)];
                var dob = DateTime.Parse(randomDob[rnd.Next(0, 3)]);
                var receiptDate = DateTime.Parse(randomReceiptDate[rnd.Next(0, 3)]);
                
                Passport passport = new Passport(name, surname, middlename, dob, receiptDate);

                string index = randomIndex[rnd.Next(0, 3)];
                string faculty = randomFaculty[rnd.Next(0, 3)];
                string speciality = randomSpeciality[rnd.Next(0, 3)];
                
                UniversityGroup universityGroup = new UniversityGroup(index, faculty, speciality);

                int progress = rnd.Next(1, 100);
                
                studentList.Add(new Student(passport, universityGroup, progress));
            }

            return studentList;
        }
    }
}