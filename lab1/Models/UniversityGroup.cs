﻿using System;

namespace lab1.Models
{
    public class UniversityGroup
    {
        private string _index;
        private string _faculty;
        private string _speciality;

        public UniversityGroup(string index, string faculty, string speciality)
        {
            _index = index;
            _faculty = faculty;
            _speciality = speciality;
        }

        public override string ToString()
        {
            return $"Индекс группы: {_index}\nФакультет: {_faculty}\nСпециальность: {_speciality}\n" ;
        }
    }
}