﻿namespace lab1.Models
{
    public class StudentsList
    {
        public Student Student;
        public StudentsList Next;

        public StudentsList(Student student, StudentsList next)
        {
            Student = student;
            Next = next;
        }

        public StudentsList()
        {
            Student = null;
            Next = null;
        }

        public void Add(Student newStudent)
        {
            Next = new StudentsList(newStudent, null);
        }
    }
}