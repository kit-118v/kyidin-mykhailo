﻿using System;
using System.Text.RegularExpressions;

namespace lab1.Models
{
    public class Student
    {
        private Passport _passport;
        private UniversityGroup _universityGroup;
        private int _progress;

        public Student(Passport passport, UniversityGroup universityGroup, int progress)
        {
            _passport = passport;
            _universityGroup = universityGroup;
            _progress = progress;
        }

        public override string ToString()
        {
            return $"{_passport}\n" +
                   $"{_universityGroup}" +
                   $"Успеваемость %: {_progress.ToString()}";
        }
        
    
    }
}